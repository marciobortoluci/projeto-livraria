package br.com.metrocamp.livraria.model;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity(value="Livros",noClassnameStored=true)
public class Livros {
	
	@Id	
	private Integer id;
	private String titulo;	
	
	
	public Livros(Integer id, String titulo) {
		this.id = id;
		this.titulo = titulo;
	}

	public List<Livros> listLivros() {
		List<Livros> listLivros = new ArrayList<Livros>();
		return listLivros;
	}	

	public Livros() {

	}
}
